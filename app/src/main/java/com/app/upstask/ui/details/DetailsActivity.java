package com.app.upstask.ui.details;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.upstask.R;
import com.app.upstask.data.models.articles.Articles;
import com.bumptech.glide.Glide;

public class DetailsActivity extends AppCompatActivity {

    private ImageView image;
    private TextView hint;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setTitle(getString(R.string.details));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getData();

    }

    private void getData() {
        Articles article = (Articles) getIntent().getSerializableExtra("article");
        setupList(article);
    }


    private void setupList(Articles article) {
        image = findViewById(R.id.iv_image);
        hint = findViewById(R.id.tv_hint);
        Glide.with(this).load(article.getMedia().get(0).getMediaMetadata().get(0).getUrl()).into(image);
        hint.setText(article.getTitle());

    }
}
