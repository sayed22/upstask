package com.app.upstask.ui.list;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.app.upstask.R;
import com.app.upstask.data.models.articles.Articles;
import com.app.upstask.ui.ArticlesViewModel;
import com.app.upstask.ui.details.DetailsActivity;

import java.util.List;

public class ArticlesActivity extends AppCompatActivity {

    private ArticlesAdapter mAdapter;
    private ArticlesViewModel viewModel;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);
        getSupportActionBar().setTitle(getString(R.string.articles));
        progressBar = findViewById(R.id.progress);
        viewModel = new ViewModelProvider(this).get(ArticlesViewModel.class);
        getData();
    }

    private void getData() {
        viewModel.getArticles();
        viewModel.articlesLiveData.observe(this, articlesResponseDataResource -> {
            progressBar.setVisibility(View.GONE);
            if (articlesResponseDataResource.getStatus().equals(articlesResponseDataResource.SUCCESS)) {
                setupList(articlesResponseDataResource.getData().getResults());
            } else {
                Toast.makeText(this, articlesResponseDataResource.getError(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupList(List<Articles> results) {
        RecyclerView recyclerView = findViewById(R.id.rv_articles);
        mAdapter = new ArticlesAdapter(results);
        mAdapter.setOnItemClickListener(item -> {

            startActivity(new Intent(this, DetailsActivity.class).putExtra("article", item));
        });
        recyclerView.setAdapter(mAdapter);
    }
}
