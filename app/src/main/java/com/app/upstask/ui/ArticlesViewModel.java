package com.app.upstask.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.upstask.data.api.ArticlesWebService;
import com.app.upstask.data.models.DataResource;
import com.app.upstask.data.models.articles.ArticlesResponse;
import com.app.upstask.data.repo.ArticlesRepo;

import io.reactivex.rxjava3.disposables.CompositeDisposable;

public class ArticlesViewModel extends ViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ArticlesRepo articlesRepo = new ArticlesRepo(new ArticlesWebService(), compositeDisposable);
    public MutableLiveData<DataResource<ArticlesResponse>> articlesLiveData = new MutableLiveData<>();

    public void getArticles() {
        articlesRepo.getMovies(7).observeForever(articlesResponseDataResource -> articlesLiveData.setValue(articlesResponseDataResource));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
