package com.app.upstask.ui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.upstask.R;
import com.app.upstask.data.models.articles.Articles;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.MyViewHolder> {
    private static OnItemClickListener clickListener;
    List<Articles> data;

    public ArticlesAdapter(List<Articles> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ArticlesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_article, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlesAdapter.MyViewHolder holder, int position) {
        Articles articles = data.get(position);
        holder.title.setText(articles.getTitle());
        if (!articles.getMedia().isEmpty())
            holder.writer.setText(articles.getMedia().get(0).getCopyright());
        holder.date.setText(articles.getPublishedDate());
        if (!articles.getMedia().isEmpty())
            Glide.with(holder.itemView.getContext()).load(articles.getMedia().get(0).getMediaMetadata().get(0).getUrl()).into(holder.image);

        holder.itemView.setOnClickListener(view -> {
            clickListener.onItemClick(articles);
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, date, writer;
        CircleImageView image;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tv_title);
            writer = view.findViewById(R.id.tv_writer);
            date = view.findViewById(R.id.tv_date);
            image = view.findViewById(R.id.iv_article_image);
        }


    }


    public void setOnItemClickListener(OnItemClickListener clickListener) {
        ArticlesAdapter.clickListener = clickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(Articles item);
    }
}
