package com.app.upstask.data.models;

public class DataResource<T> {

    private T data;
    private String status;
    private String error;


    public T getData() {
        return data;
    }

    public void setData(T subjects) {
        this.data = subjects;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

   public final String SUCCESS = "success";
   public final String FAILED = "failed";
}
