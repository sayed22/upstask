package com.app.upstask.data.api;

import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
 import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkModule {
    private static final int DEFAULT_TIMEOUT = 10;
    private final String BASE_URL = "https://api.nytimes.com/svc/mostpopular/v2/";
    private Retrofit retrofit;
    private OkHttpClient.Builder builder;

    private static class Singleton {
        private static final NetworkModule INSTANCE = new NetworkModule();
    }

    public static NetworkModule getInstance() {
        return Singleton.INSTANCE;
    }

    Retrofit getRetrofit() {
        builder = new OkHttpClient.Builder();
        builder.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        return retrofit;
    }

}
