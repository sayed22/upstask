package com.app.upstask.data.api;

import com.app.upstask.data.models.articles.ArticlesResponse;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticlesApi {

    @GET("viewed/{start}.json")
    Observable<ArticlesResponse> getArticles(@Path("start") int start, @Query("api-key") String key);
}
