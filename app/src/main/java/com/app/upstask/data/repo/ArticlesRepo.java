package com.app.upstask.data.repo;

import androidx.lifecycle.MutableLiveData;

import com.app.upstask.data.api.ArticlesWebService;
import com.app.upstask.data.models.DataResource;
import com.app.upstask.data.models.articles.ArticlesResponse;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ArticlesRepo {

    public ArticlesRepo(ArticlesWebService articlesWebService, CompositeDisposable compositeDisposable) {
        this.articlesWebService = articlesWebService;
        this.compositeDisposable = compositeDisposable;
    }

    private CompositeDisposable compositeDisposable;
    private ArticlesWebService articlesWebService;
    private MutableLiveData<DataResource<ArticlesResponse>> liveData = new MutableLiveData<>();
    private DataResource<ArticlesResponse> dataResource = new DataResource<>();

    public MutableLiveData<DataResource<ArticlesResponse>> getMovies(int start) {
        compositeDisposable.add(articlesWebService.getArticles(start)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArticlesResponse>() {
                    @Override
                    public void onComplete() {
                        dataResource.setStatus(dataResource.SUCCESS);
                        liveData.postValue(dataResource);
                    }

                    @Override
                    public void onError(Throwable e) {
                        dataResource.setStatus(dataResource.FAILED);
                        dataResource.setError(e.toString());
                    }

                    @Override
                    public void onNext(ArticlesResponse value) {
                        dataResource.setData(value);
                    }
                }));
        return liveData;
    }
}
