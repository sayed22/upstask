package com.app.upstask.data.api;

import com.app.upstask.data.models.articles.ArticlesResponse;

import io.reactivex.rxjava3.core.Observable;

public class ArticlesWebService {

    public Observable<ArticlesResponse> getArticles(int start) {
        return NetworkModule.getInstance().getRetrofit().create(ArticlesApi.class).getArticles(start, "lmjZGeh5FdA9clVqTCWn01syTzkKllUN");
    }

}
